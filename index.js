let express = require('express');
const app = express();
const basicAuth = require('express-basic-auth');

const port = process.env.POLKA_SCANNER_PORT || 8080
const username = process.env.POLKA_USERNAME
const pwd = process.env.POLKA_PWD

if (!username || !pwd) {
  console.error('Set POLKA_USERNAME / POLKA_PWD before start server!')
}

app.use(basicAuth({
  users: { [username]: pwd },
  challenge: true
}));

app.use('/', express.static('public'))

app.listen(port, () => {
  console.log(`App listening at port:${port}`)
})