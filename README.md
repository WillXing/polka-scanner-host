# Scanner Host

This is just a simple node server hosting static Polkadot scanner.

## Docker

Dockerfile only use to run service on the VPC. If debug or run in local, easier way is just use node to run it.

## Environment variables

- POLKA_SCANNER_PORT: port the server listen on
- POLKA_USERNAME: username for basic authentication
- POLKA_PWD: password for basic authentication