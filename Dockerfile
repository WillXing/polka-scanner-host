FROM node:lts-alpine3.10

RUN npm install pm2 -g

WORKDIR /app

COPY . /app

RUN npm install

CMD pm2-runtime start /app/index.js